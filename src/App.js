import React, { Component } from 'react';
import { BrowserRouter as Router, Link, Route } from 'react-router-dom';
// import RaisedButton from 'material-ui/RaisedButton';
// import logo from './logo.svg';
import './App.css';
import Navbar from './Navbar.js'
import Projects from './Projects.js'

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <Navbar/>
          {/*<header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <h1 className="App-title">Welcome to React</h1>
          </header>*/}
          {/*<MuiThemeProvider>
            <Route path="/projects" exact={true} component={Projects}/>
          </MuiThemeProvider>*/}

          <div className="content">
            <Link to="/test/1234">
              <h1>Heading 1</h1>
            </Link>
            <Link to="/">
              <h2>Heading 2</h2>
            </Link>
            <h3>Heading 3</h3>

            <Link to="/projects">
              <h3>Projects</h3>
            </Link>

            <Route path="/" exact={true} render={() => (<h1>Welcome!</h1>)}/>
            <Route path="/test/:testid" component={TestComp}/>
            <Route path="/projects" exact={true} component={Projects}/>
          </div>
        </div>
      </Router>
    );
  }
}

const TestComp = ({ match }) => (
  <div>
    Test Thing! - {match.params.testid}
  </div>
)

export default App;
